package liou.rayyuan.hivoicetube

interface Presenter<in V> {
    fun attachView(view: V)
    fun detachView()
}