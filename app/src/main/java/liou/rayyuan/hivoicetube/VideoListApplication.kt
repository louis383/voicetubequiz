package liou.rayyuan.hivoicetube

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

class VideoListApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
    }
}