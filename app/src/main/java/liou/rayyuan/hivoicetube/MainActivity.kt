package liou.rayyuan.hivoicetube

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import liou.rayyuan.hivoicetube.counter.CounterActivity
import liou.rayyuan.hivoicetube.settings.SettingsActivity
import liou.rayyuan.hivoicetube.videolist.VideoListActivity

class MainActivity : AppCompatActivity() {
    private val networkQuestionButton: Button by bindView(R.id.main_activity_question1_button)
    private val preferenceQuestionButton: Button by bindView(R.id.main_activity_question2_button)
    private val counterQuestionButton: Button by bindView(R.id.main_activity_question3_button)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        networkQuestionButton.setOnClickListener {
            val intent = Intent(this, VideoListActivity::class.java)
            startActivity(intent)
        }

        preferenceQuestionButton.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }

        counterQuestionButton.setOnClickListener {
            val intent = Intent(this, CounterActivity::class.java)
            startActivity(intent)
        }
    }

    private fun <T: View> Activity.bindView(@IdRes resId: Int): Lazy<T> = lazy {
        findViewById<T>(resId)
    }
}
