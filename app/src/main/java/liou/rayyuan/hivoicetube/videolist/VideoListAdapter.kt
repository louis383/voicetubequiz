package liou.rayyuan.hivoicetube.videolist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import liou.rayyuan.hivoicetube.R
import liou.rayyuan.hivoicetube.data.entity.Video

class VideoListAdapter: RecyclerView.Adapter<VideoListAdapter.VideoResultViewHolder>() {

    private val videos: List<Video> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoResultViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.video_list_item, parent, false)
        return VideoResultViewHolder(view)
    }

    override fun getItemCount(): Int = videos.size

    override fun onBindViewHolder(holder: VideoResultViewHolder, position: Int) {
        val index = holder.adapterPosition
        if (videos.isNotEmpty()) {
            holder.cover.setImageURI(videos[index].img)
            holder.title.text = videos[index].title
        }
    }

    fun addVideos(videos: List<Video>) {
        val originIndex = this.videos.size
        (this.videos as ArrayList).addAll(videos)
        notifyItemRangeInserted(originIndex, videos.size)
    }

    fun clean() {
        (videos as ArrayList).clear()
        notifyDataSetChanged()
    }

    class VideoResultViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        internal val title: TextView = itemView.findViewById(R.id.video_item_title)
        internal val cover: SimpleDraweeView = itemView.findViewById(R.id.video_item_cover)
    }
}