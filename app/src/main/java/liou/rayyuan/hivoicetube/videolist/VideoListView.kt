package liou.rayyuan.hivoicetube.videolist

import android.content.Context
import liou.rayyuan.hivoicetube.ViewState

interface VideoListView {
    fun setVideoListUI(state: ViewState)
    fun setErrorText(text: String)
    fun readyToDoNextLoad()
    fun getContext(): Context
}