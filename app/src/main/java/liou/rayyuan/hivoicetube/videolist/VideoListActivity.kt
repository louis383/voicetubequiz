package liou.rayyuan.hivoicetube.videolist

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import liou.rayyuan.hivoicetube.R
import liou.rayyuan.hivoicetube.ViewState
import liou.rayyuan.hivoicetube.utils.OnMoreListener
import liou.rayyuan.hivoicetube.utils.RecyclerViewLoadMoreHelper

class VideoListActivity: AppCompatActivity(), VideoListView, OnMoreListener {

    private val videoRecyclerView: RecyclerView by bindView(R.id.activity_video_list_recyclerview)
    private val videoSwipeRefreshView: SwipeRefreshLayout by bindView(R.id.activity_video_list_refresh_layout)
    private val progressBar: ProgressBar by bindView(R.id.activity_video_list_progressBar)
    private val statusText: TextView by bindView(R.id.activity_video_list_status_text)

    private lateinit var recyclerViewMoreHelper: RecyclerViewLoadMoreHelper
    private val presenter: VideoListPresenter = VideoListPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_list)
        videoSwipeRefreshView.isEnabled = false    // disable swipe to refresh

        presenter.attachView(this)
        presenter.setupRecylcerView(videoRecyclerView)

        recyclerViewMoreHelper = RecyclerViewLoadMoreHelper(this, videoRecyclerView)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    //region VideoListView
    override fun setVideoListUI(state: ViewState) {
        when (state) {
            ViewState.PREPARING -> {
                progressBar.visibility = View.VISIBLE
                videoRecyclerView.visibility = View.GONE
                statusText.visibility = View.GONE
            }
            ViewState.READY -> {
                progressBar.visibility = View.GONE
                videoRecyclerView.visibility = View.VISIBLE
                statusText.visibility = View.GONE
            }
            ViewState.EMPTY -> {
                progressBar.visibility = View.GONE
                videoRecyclerView.visibility = View.GONE
                statusText.visibility = View.VISIBLE
                statusText.text = getString(R.string.video_list_is_empty)
            }
            ViewState.ERROR -> {
                progressBar.visibility = View.GONE
                videoRecyclerView.visibility = View.GONE
                statusText.visibility = View.VISIBLE
                statusText.text = getString(R.string.video_list_is_error)
            }
        }
    }

    override fun setErrorText(text: String) {
        statusText.text = text
    }

    override fun readyToDoNextLoad() {
        recyclerViewMoreHelper.readyForNextLoad()
    }

    override fun getContext(): Context = this
    //endregion

    //region OnMoreListener
    override fun onMoreAsked() {
        Log.i("VideoListActivity", "onMoreAsked")
        presenter.requestMoreVideo()
    }
    //endregion

    private fun <T: View> Activity.bindView(@IdRes resId: Int): Lazy<T> = lazy {
        findViewById<T>(resId)
    }
}