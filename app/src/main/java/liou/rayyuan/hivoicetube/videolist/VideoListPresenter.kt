package liou.rayyuan.hivoicetube.videolist

import android.support.v7.widget.RecyclerView
import liou.rayyuan.hivoicetube.Presenter
import liou.rayyuan.hivoicetube.ViewState
import liou.rayyuan.hivoicetube.data.VideoRepository
import liou.rayyuan.hivoicetube.data.VideoRepositoryCallback
import liou.rayyuan.hivoicetube.data.entity.Video

class VideoListPresenter: Presenter<VideoListView>, VideoRepositoryCallback {

    lateinit var view: VideoListView

    private lateinit var videoRepository: VideoRepository
    private val adapter: VideoListAdapter = VideoListAdapter()

    override fun attachView(view: VideoListView) {
        this.view = view

        videoRepository = VideoRepository(view.getContext(), this)

        view.setVideoListUI(ViewState.PREPARING)
        requestMoreVideo()
    }

    override fun detachView() {
        videoRepository.cancel()
    }

    fun setupRecylcerView(recyclerView: RecyclerView) {
        recyclerView.adapter = adapter
    }

    fun requestMoreVideo() {
        videoRepository.getVideos()
    }

    //region VideoRepositoryCallback
    override fun onVideoListResultCallback(result: List<Video>?) {
        if (result == null) {
            view.setVideoListUI(ViewState.ERROR)
            return
        }

        if (result.isNotEmpty()) {
            adapter.addVideos(result)
            view.setVideoListUI(ViewState.READY)
            view.readyToDoNextLoad()
        } else {
            view.setVideoListUI(ViewState.EMPTY)
        }
    }

    override fun onVideoListErrorOccurred(message: String?) {
        val result = message ?: ""
        view.setVideoListUI(ViewState.ERROR)
        view.setErrorText(result)
    }
    //endregion
}