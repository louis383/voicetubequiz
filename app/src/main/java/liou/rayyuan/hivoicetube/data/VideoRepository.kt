package liou.rayyuan.hivoicetube.data

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import liou.rayyuan.hivoicetube.data.entity.Video
import liou.rayyuan.hivoicetube.data.entity.VideoList
import liou.rayyuan.hivoicetube.data.threads.DBWorkerThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VideoRepository(context: Context, private var callback: VideoRepositoryCallback?) {
    private val key = "VoiceTube"
    private val apiManager: APIManager = APIManager()
    private var videoInfoDatabase: VideoInfoDatabase? = null

    private lateinit var networkCall: Call<VideoList>
    private val workerThread: DBWorkerThread
    private val uiThread: Handler

    private var restoredFromCache: Boolean = false

    init {
        videoInfoDatabase = VideoInfoDatabase.getInstance(context)
        uiThread = Handler(Looper.getMainLooper())

        workerThread = DBWorkerThread("DatabaseThread")
        workerThread.start()
    }

    fun getVideos() {
        if (!restoredFromCache) {
            queryVideosFromLocal()
        } else {
            fetchVideos()
        }
    }

    private fun queryVideosFromLocal() {
        val task = Runnable {
            val videos: List<Video>? = videoInfoDatabase?.videoDao()?.getAllCachedVideos()

            uiThread.post({
                if (videos != null && videos.isNotEmpty()) {
                    callback?.onVideoListResultCallback(videos)
                    Log.i("VideoRepository", "videos from local")
                } else {
                    fetchVideos()
                }

                restoredFromCache = true
            })
        }

        workerThread.postTask(task)
    }

    private fun fetchVideos() {
        networkCall = apiManager.postVideoList(key)
        networkCall.enqueue(object : Callback<VideoList> {
            override fun onResponse(call: Call<VideoList>?, response: Response<VideoList>?) {
                if (response == null) {
                    return
                }

                if (response.isSuccessful) {
                    callback?.onVideoListResultCallback(response.body()?.videos)
                    writeIntoLocal(response.body()?.videos)
                } else {
                    val message = response.errorBody()?.string()
                    callback?.onVideoListErrorOccurred(message)
                }
            }

            override fun onFailure(call: Call<VideoList>?, throwable: Throwable?) {
                Log.e("VideoRepository", Log.getStackTraceString(throwable))
                val message = throwable?.localizedMessage
                callback?.onVideoListErrorOccurred(message)
            }
        })
    }

    private fun writeIntoLocal(videos: List<Video>?) {
        if (videos != null) {
            val task = Runnable {
                for (video in videos) {
                    videoInfoDatabase?.videoDao()?.insertVideos(video)
                }
                Log.i("VideoRepository", "write in database")
            }
            workerThread.postTask(task)
        }
    }

    fun cancel() {
        callback = null

        if (this::networkCall.isInitialized) {
            networkCall.cancel()
        }

        VideoInfoDatabase.releaseInstance()
        workerThread.quit()
    }
}