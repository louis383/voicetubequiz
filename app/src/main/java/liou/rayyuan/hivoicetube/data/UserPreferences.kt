package liou.rayyuan.hivoicetube.data

import android.content.Context
import android.content.SharedPreferences

class UserPreferences(context: Context) {
    private val userPreferenceName = "user-preference-tag"
    private val videoAutoPlayKey = "video-auto-play-key"
    private val subtitleSyncingKey = "subtitle-syncing-key"
    private val stopPlayingWhenConsultingKey = "stop-playing-when-consulting-key"
    private val videoSuggestionsKey = "video-suggestions-key"
    private val learningNotificationKey = "learning-notification-key"

    private val preferences: SharedPreferences

    init {
        preferences = context.getSharedPreferences(userPreferenceName, Context.MODE_PRIVATE)
    }

    fun setVideoAutoPlayEnable(enable: Boolean) {
        preferences.edit()
                .putBoolean(videoAutoPlayKey, enable)
                .apply()
    }

    fun isVideoAutoPlayEnabled(): Boolean = preferences.getBoolean(videoAutoPlayKey, false)

    fun setSubtitleSyncingEnable(enable: Boolean) {
        preferences.edit()
                .putBoolean(subtitleSyncingKey, enable)
                .apply()
    }

    fun isSubtitleSyncingEnabled(): Boolean = preferences.getBoolean(subtitleSyncingKey, false)

    fun setStopPlayingWhenConsultingEnable(enable: Boolean) {
        preferences.edit()
                .putBoolean(stopPlayingWhenConsultingKey, enable)
                .apply()
    }

    fun isStopPlayingWhenConsultingEnabled(): Boolean = preferences.getBoolean(stopPlayingWhenConsultingKey, false)

    fun setVideoSuggestionEnable(enable: Boolean) {
        preferences.edit()
                .putBoolean(videoSuggestionsKey, enable)
                .apply()
    }

    fun isVideoSuggestingEnabled(): Boolean = preferences.getBoolean(videoSuggestionsKey, false)

    fun setLearningNotificationEnable(enable: Boolean) {
        preferences.edit()
                .putBoolean(learningNotificationKey, enable)
                .apply()
    }

    fun isLearningNotificationEnabled(): Boolean = preferences.getBoolean(learningNotificationKey, false)
}

