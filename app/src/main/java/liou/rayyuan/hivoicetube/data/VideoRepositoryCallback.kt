package liou.rayyuan.hivoicetube.data

import liou.rayyuan.hivoicetube.data.entity.Video

interface VideoRepositoryCallback {
    fun onVideoListResultCallback(result: List<Video>?)
    fun onVideoListErrorOccurred(message: String?)
}