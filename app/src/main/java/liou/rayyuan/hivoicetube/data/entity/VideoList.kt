package liou.rayyuan.hivoicetube.data.entity
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName

/*
 * sample json
{
    "status": "success",
    "videos": [
        {
            "title": "IGN News - Kiefer Sutherland Playing Snake in Metal Gear Solid V",
            "img": "http://img.youtube.com/vi/S-FFgpUfVX8/maxresdefault.jpg"
        },
        {
            "title": "Taylor Swift - New Romantics",
            "img": "http://img.youtube.com/vi/wyK7YuwUWsU/maxresdefault.jpg"
        },
        {
            "title": "Great White Sharks 360 Video 4K!! - Close encounter on Amazing Virtual Dive",
            "img": "http://img.youtube.com/vi/HNOT_feL27Y/maxresdefault.jpg"
        }
    ]
}
 */

data class VideoList(
		@SerializedName("status") val status: String?,
		@SerializedName("videos") val videos: List<Video>?
)

@Entity(tableName = "video_info")
data class Video(
		@NonNull @PrimaryKey(autoGenerate = true) val id: Int,
		@SerializedName("title") val title: String?,
		@ColumnInfo(name = "image") @SerializedName("img") val img: String?
)