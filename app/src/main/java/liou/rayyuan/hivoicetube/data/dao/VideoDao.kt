package liou.rayyuan.hivoicetube.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import liou.rayyuan.hivoicetube.data.entity.Video

@Dao
interface VideoDao {

    @Query("SELECT * FROM video_info")
    fun getAllCachedVideos(): List<Video>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVideos(vararg video: Video)

    @Query("DELETE FROM video_info")
    fun deleteAll()

    @Query("SELECT count(*) FROM video_info")
    fun getCacheCounts(): Int
}