package liou.rayyuan.hivoicetube.data.threads

import android.os.Handler
import android.os.HandlerThread

class DBWorkerThread(name: String) : HandlerThread(name) {

    private lateinit var workerThread: Handler

    override fun onLooperPrepared() {
        super.onLooperPrepared()
        workerThread = Handler(looper)
    }

    fun postTask(task: Runnable) {
        if (!this::workerThread.isInitialized) {
            workerThread = Handler(looper)
        }
        workerThread.post(task)
    }
}
