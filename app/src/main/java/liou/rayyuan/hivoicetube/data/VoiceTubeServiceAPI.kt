package liou.rayyuan.hivoicetube.data

import liou.rayyuan.hivoicetube.data.entity.VideoList
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface VoiceTubeServiceAPI {

    // Just not feel that RESTful...
    @FormUrlEncoded
    @POST("thirdparty/test.php")
    fun postVideos(@Field("key", encoded = true) key: String): Call<VideoList>

}