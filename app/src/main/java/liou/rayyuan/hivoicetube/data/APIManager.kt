package liou.rayyuan.hivoicetube.data

import liou.rayyuan.hivoicetube.BuildConfig
import liou.rayyuan.hivoicetube.data.entity.VideoList
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class APIManager {

    private val voiceTubeService: VoiceTubeServiceAPI
    private val timeout: Long = 60
    private val hostURL = "https://api.voicetube.com/"

    init {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.HEADERS
        if (BuildConfig.DEBUG) {
            logInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }

        val httpClient = OkHttpClient.Builder()
                .addInterceptor(logInterceptor)
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .readTimeout(timeout, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(hostURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build()

        voiceTubeService = retrofit.create(VoiceTubeServiceAPI::class.java)
    }

    fun postVideoList(key: String): Call<VideoList> {
        return voiceTubeService.postVideos(key)
    }
}