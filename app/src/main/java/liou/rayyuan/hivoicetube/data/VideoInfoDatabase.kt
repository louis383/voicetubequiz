package liou.rayyuan.hivoicetube.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import liou.rayyuan.hivoicetube.data.dao.VideoDao
import liou.rayyuan.hivoicetube.data.entity.Video

@Database(entities = [(Video::class)], version = 1)
abstract class VideoInfoDatabase: RoomDatabase() {

    abstract fun videoDao(): VideoDao

    companion object {
        private const val databaseName = "video_database"
        private var INSTANCE: VideoInfoDatabase? = null

        fun getInstance(context: Context): VideoInfoDatabase? {
            if (INSTANCE == null) {
                synchronized(VideoInfoDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            VideoInfoDatabase::class.java,
                            databaseName)
                            .build()
                }
            }

            return INSTANCE
        }

        fun releaseInstance() {
            INSTANCE = null
        }
    }
}