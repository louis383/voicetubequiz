package liou.rayyuan.hivoicetube

enum class ViewState {
    PREPARING, READY, EMPTY, ERROR
}