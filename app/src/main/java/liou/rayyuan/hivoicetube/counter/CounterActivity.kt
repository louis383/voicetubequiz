package liou.rayyuan.hivoicetube.counter

import android.app.Activity
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import liou.rayyuan.hivoicetube.R

class CounterActivity: AppCompatActivity(), CounterView {

    private val countingNumberTag: String = "counting-number-tag"

    private val countDownNumber: TextView by bindView(R.id.counter_timer)
    private val countDownButton: Button by bindView(R.id.counter_counting_button)
    private val countDownEditText: EditText by bindView(R.id.counter_setting_edittext)

    private val presenter = CounterPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_counter)
        presenter.attachView(this)

        countDownButton.setOnClickListener {
            val inputNumber = countDownEditText.text.toString()
            if (inputNumber.isNotEmpty()) {
                val formattedNumberText = inputNumber.replaceFirst("^0+(?!$)", "")
                presenter.startCountDown(Integer.parseInt(formattedNumberText))
            }
        }

        if (savedInstanceState != null) {
            presenter.remainingNumber = savedInstanceState.getInt(countingNumberTag)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.resumeTimer()
    }

    override fun onPause() {
        super.onPause()
        presenter.pauseTimer()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(countingNumberTag, presenter.remainingNumber)
    }

    //region CounterView
    override fun updateNumber(number: Int) {
        countDownNumber.text = number.toString()
    }
    //endregion

    private fun <T: View> Activity.bindView(@IdRes resId: Int): Lazy<T> = lazy {
        findViewById<T>(resId)
    }
}