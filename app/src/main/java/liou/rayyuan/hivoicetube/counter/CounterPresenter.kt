package liou.rayyuan.hivoicetube.counter

import android.os.CountDownTimer
import liou.rayyuan.hivoicetube.Presenter

class CounterPresenter: Presenter<CounterView> {

    private lateinit var view: CounterView
    private lateinit var countDownTimer: CountDownTimer

    var remainingNumber: Int = 0

    override fun attachView(view: CounterView) {
        this.view = view
        init()
    }

    override fun detachView() {}

    private fun init() {
        view.updateNumber(0)
    }

    fun pauseTimer() {
        if (this::countDownTimer.isInitialized) {
            countDownTimer.cancel()
        }
    }

    fun resumeTimer() {
        if (remainingNumber > 0) {
            startCountDown(remainingNumber)
        }
    }

    fun startCountDown(startNumber: Int) {
        if (startNumber <= 0) {
            return
        }

        if (this::countDownTimer.isInitialized) {
            countDownTimer.cancel()
            view.updateNumber(0)
        }

        view.updateNumber(startNumber)
        countDownTimer = object : CountDownTimer((startNumber * 1000).toLong(), 1000) {
            override fun onFinish() {
                view.updateNumber(0)
            }

            override fun onTick(number: Long) {
                remainingNumber = (number / 1000).toInt()
                view.updateNumber(remainingNumber)
            }
        }.start()
    }
}