package liou.rayyuan.hivoicetube.counter

interface CounterView {
    fun updateNumber(number: Int)
}