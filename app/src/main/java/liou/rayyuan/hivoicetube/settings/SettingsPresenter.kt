package liou.rayyuan.hivoicetube.settings

import liou.rayyuan.hivoicetube.Presenter
import liou.rayyuan.hivoicetube.data.UserPreferences

class SettingsPresenter(view: SettingsView, private val userPreferences: UserPreferences):
        Presenter<SettingsView> {

    val view: SettingsView

    init {
        view.setupPresenter(this)

        this.view = view
    }

    override fun attachView(view: SettingsView) {}

    override fun detachView() {}

    fun start() {
        view.checkAutoPlaySwitch(userPreferences.isVideoAutoPlayEnabled())
        view.checkSubtitleSyncSwitch(userPreferences.isSubtitleSyncingEnabled())
        view.checkStopPlayWhenConsultSwitch(userPreferences.isStopPlayingWhenConsultingEnabled())
        view.checkVideoSuggestionSwitch(userPreferences.isVideoSuggestingEnabled())
        view.checkLearningNotificationSwitch(userPreferences.isLearningNotificationEnabled())
    }

    fun updateAutoPaySetting(enable: Boolean) {
        userPreferences.setVideoAutoPlayEnable(enable)
    }

    fun updateSubtitleSyncSetting(enable: Boolean) {
        userPreferences.setSubtitleSyncingEnable(enable)
    }

    fun updateStopPlayingWhenConsultSetting(enable: Boolean) {
        userPreferences.setStopPlayingWhenConsultingEnable(enable)
    }

    fun updateVideoSuggestionSetting(enable: Boolean) {
        userPreferences.setVideoSuggestionEnable(enable)
    }

    fun updateNotificationSetting(enable: Boolean) {
        userPreferences.setLearningNotificationEnable(enable)
    }
}