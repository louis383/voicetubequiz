package liou.rayyuan.hivoicetube.settings

interface SettingsView {
    fun setupPresenter(presenter: SettingsPresenter)
    fun checkAutoPlaySwitch(enable: Boolean)
    fun checkSubtitleSyncSwitch(enable: Boolean)
    fun checkStopPlayWhenConsultSwitch(enable: Boolean)
    fun checkVideoSuggestionSwitch(enable: Boolean)
    fun checkLearningNotificationSwitch(enable: Boolean)
}