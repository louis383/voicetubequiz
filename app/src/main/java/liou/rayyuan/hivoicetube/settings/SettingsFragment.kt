package liou.rayyuan.hivoicetube.settings

import android.os.Bundle
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.preference.SwitchPreferenceCompat
import android.util.Log
import liou.rayyuan.hivoicetube.R

class SettingsFragment: PreferenceFragmentCompat(), SettingsView,
        Preference.OnPreferenceChangeListener {

    private val autoPlayPreferenceKey = "preference_auto_play"
    private val subtitleSyncPreferenceKey = "preference_subtitles_syncing"
    private val stopPlayingWhenConsultKey = "preference_stop_playing_when_consulting"
    private val videoSuggestionKey = "preference_video_suggestion_notification"
    private val learningNotificationKey = "preference_learning_notification"

    private lateinit var presenter: SettingsPresenter
    private lateinit var autoPlaySwitch: SwitchPreferenceCompat
    private lateinit var subtitleSyncingSwitch: SwitchPreferenceCompat
    private lateinit var stopPlayingWhenConsultingSwitch: SwitchPreferenceCompat
    private lateinit var videoSuggestionsSwitch: SwitchPreferenceCompat
    private lateinit var learningNotificationSwitch: SwitchPreferenceCompat

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        Log.i("SettingsFragment", "onCreatePreferences")
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }

    override fun onResume() {
        super.onResume()
        autoPlaySwitch = findPreference(autoPlayPreferenceKey) as SwitchPreferenceCompat
        subtitleSyncingSwitch = findPreference(subtitleSyncPreferenceKey) as SwitchPreferenceCompat
        stopPlayingWhenConsultingSwitch = findPreference(stopPlayingWhenConsultKey) as SwitchPreferenceCompat
        videoSuggestionsSwitch = findPreference(videoSuggestionKey) as SwitchPreferenceCompat
        learningNotificationSwitch = findPreference(learningNotificationKey) as SwitchPreferenceCompat

        presenter.start()
        registerListeners()
    }

    override fun onPause() {
        super.onPause()
        unregisterListeners()
    }

    private fun registerListeners() {
        autoPlaySwitch.onPreferenceChangeListener = this
        subtitleSyncingSwitch.onPreferenceChangeListener = this
        stopPlayingWhenConsultingSwitch.onPreferenceChangeListener = this
        videoSuggestionsSwitch.onPreferenceChangeListener = this
        learningNotificationSwitch.onPreferenceChangeListener = this
    }

    private fun unregisterListeners() {
        autoPlaySwitch.onPreferenceChangeListener = null
        subtitleSyncingSwitch.onPreferenceChangeListener = null
        stopPlayingWhenConsultingSwitch.onPreferenceChangeListener = null
        videoSuggestionsSwitch.onPreferenceChangeListener = null
        learningNotificationSwitch.onPreferenceChangeListener = null
    }

    //region SettingsView
    override fun setupPresenter(presenter: SettingsPresenter) {
        this.presenter = presenter
    }

    override fun checkAutoPlaySwitch(enable: Boolean) {
        autoPlaySwitch.isChecked = enable
    }

    override fun checkSubtitleSyncSwitch(enable: Boolean) {
        subtitleSyncingSwitch.isChecked = enable
    }

    override fun checkStopPlayWhenConsultSwitch(enable: Boolean) {
        stopPlayingWhenConsultingSwitch.isChecked = enable
    }

    override fun checkVideoSuggestionSwitch(enable: Boolean) {
        videoSuggestionsSwitch.isChecked = enable
    }

    override fun checkLearningNotificationSwitch(enable: Boolean) {
        learningNotificationSwitch.isChecked = enable
    }
    //endregion

    //region Preference.OnPreferenceChangeListener
    override fun onPreferenceChange(preference: Preference?, any: Any?): Boolean {
        Log.i("SettingsFragment", "${preference?.key}, value = $any")
        if (!this::presenter.isInitialized) {
            return false
        }

        when (preference?.key) {
            autoPlayPreferenceKey -> presenter.updateAutoPaySetting(any as Boolean)
            subtitleSyncPreferenceKey -> presenter.updateSubtitleSyncSetting(any as Boolean)
            stopPlayingWhenConsultKey -> presenter.updateStopPlayingWhenConsultSetting(any as Boolean)
            videoSuggestionKey -> presenter.updateVideoSuggestionSetting(any as Boolean)
            learningNotificationKey -> presenter.updateNotificationSetting(any as Boolean)
        }
        return true
    }
    //endregion
}