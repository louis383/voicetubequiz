package liou.rayyuan.hivoicetube.settings

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import liou.rayyuan.hivoicetube.R
import liou.rayyuan.hivoicetube.data.UserPreferences

class SettingsActivity: AppCompatActivity() {

    private val settingsPageTag = "settings-page-tag"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.simple_container)

        val settingsFragment = findSettingFragment()
        SettingsPresenter(settingsFragment, UserPreferences(this))

        supportFragmentManager.beginTransaction()
                .replace(R.id.simple_container_rootview, settingsFragment, settingsPageTag)
                .commit()
    }

    private fun findSettingFragment(): SettingsFragment {
        val settingsFragment = supportFragmentManager.findFragmentByTag(settingsPageTag) as? SettingsFragment
        if (settingsFragment != null) {
            return settingsFragment
        }

        return SettingsFragment()
    }

}