package liou.rayyuan.hivoicetube.utils

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import java.lang.ref.WeakReference

class RecyclerViewLoadMoreHelper(private val onMoreListener: OnMoreListener,
                                 recyclerView: RecyclerView) {
    private val recyclerView: WeakReference<RecyclerView> = WeakReference(recyclerView)
    private val maxCountToAskLoadMore: Int = 10

    private var isLoadingMore: Boolean = false

    init {
        initialize()
    }

    private fun initialize() {
        this.recyclerView.get()?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                processLoadMore()
            }
        })
    }

    private fun processLoadMore() {
        val recyclerView: RecyclerView? = this.recyclerView.get()
        if (recyclerView != null) {
            val layoutManager: RecyclerView.LayoutManager = recyclerView.layoutManager
            val lastVisibleItemPosition: Int = getLastVisibleItemPosition(layoutManager)
            val totalItemCount: Int = layoutManager.itemCount

            val offscreenNextItemPosition = totalItemCount - lastVisibleItemPosition
            if (offscreenNextItemPosition <= maxCountToAskLoadMore && !isLoadingMore) {
                isLoadingMore = true
                onMoreListener.onMoreAsked()
                Log.i("RecyclerViewLoadMoreHel", "totalItemCount = $totalItemCount, lastVisibleItemPosition = $lastVisibleItemPosition")
            }
        }
    }

    fun readyForNextLoad() {
        isLoadingMore = false
    }

    /**
     * Only implement LinearLayoutManager condition because this is just
     * a demo application.
     *
     * @param layoutManager
     * @return Int: -1 means unsupported layoutManager. Only support `LinearLayoutManager` currently
     */
    private fun getLastVisibleItemPosition(layoutManager: RecyclerView.LayoutManager): Int {
        if (layoutManager is LinearLayoutManager) {
            return layoutManager.findLastVisibleItemPosition()
        }
        return -1
    }
}